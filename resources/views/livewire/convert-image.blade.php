
        <div class="container">
            <div class="row justify-content-center">
                
                    @if(!empty($photo_convert))
                        @foreach($photo_convert as $item)
                            <div class="col-3 mt-5 text-center">
                                <img src="{{ asset($item['url']); }}" alt="2" height="350px" width="255px"/>
                                <a class="mt-2" href="{{ asset($item['url']); }}" download> Tải ảnh </a>
                            </div>
                        @endforeach
                    @else
                    <div class="col-3 mt-5">
                        <img src="{{ asset('/bg/bg1.png'); }}" alt="" height="350px" width="100%"/>
                    </div>
                    <div class="col-3 mt-5">
                        <img src="{{ asset('/bg/bg2.png'); }}" alt="" height="350px" width="100%"/>
                    </div>
                    <div class="col-3 mt-5">
                        <img src="{{ asset('/bg/bg3.png'); }}" alt="3" height="350px" width="255px"/>
                    </div>
                    @endif
                
                
            </div>
            <div class="row justify-content-center pt-4">
                <div class="form-group col-4">
                    <input type="file" wire:model.lazy="photo" accept="image/*" name="image">
                </div>
            </div>
        </div>

