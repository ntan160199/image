<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $a = null;
        if(request()->image){
            $a = request()->image;
            dd($a);
        }
        return view('index',compact('a'));
    }
}
