<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Image;
use Illuminate\Support\Str;

class ConvertImage extends Component
{
    use WithFileUploads;
    public $photo;
    public $photo_convert;
    public function render()
    {
        if(!empty($this->photo)){
            $photos_insert=[];
            $bg1 = Image::make(public_path('bg/bg1.png'));
            $bg2 = Image::make(public_path('bg/bg2.png'));
            $bg3 = Image::make(public_path('bg/bg3.png'));

            $path_file_tmp   = $this->photo->getRealPath();
            
            for($i = 1;$i < 4; $i++){

                $file_name_rand  = Str::random(32);
                $file_name       = $file_name_rand . '.jpg';

                $img = Image::make($path_file_tmp);

                if($i == 1){
                    $img->resize( $bg1->width(),$bg1->height() );

                    $img->insert(public_path('bg/bg1.png'),'center');
                }elseif($i == 2){
                    $img->resize( $bg2->width(),$bg2->height() );

                    $img->insert(public_path('bg/bg2.png'),'center');
                }elseif($i == 3){
                    $img->resize( $bg3->width(),$bg3->height() );

                    $img->insert(public_path('bg/bg3.png'),'center');
                }

                $img_original = clone $img;

                $img_original->save(storage_path('app/public/' . $file_name), 95, 'jpg');

                $photos_insert[] = [
                    'url'   => 'storage/' . $img_original->basename,
                ];
            }
            $this->photo_convert  = $photos_insert;
            
        }
        return view('livewire.convert-image')
            ->extends('layouts.app')
            ->section('content');
    }
}
